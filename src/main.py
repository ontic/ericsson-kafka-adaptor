import ast
import json
import operator
import time

import redis as redis_py
from kafka import KafkaConsumer

from rest_generator import connect

with open('config.json') as data_file:
    config = json.load(data_file)


def set_up_connections():
    print "INFO: Setting up Kafka..."
    kafka = KafkaConsumer(config['kafka_topic'], bootstrap_servers=config['kafka_url'])
    print "INFO: Setting up Redis..."
    redis = redis_py.StrictRedis(host=config['redis_ip'], port=config['redis_port'])
    return kafka, redis


def ensure_shares(shares):
    while True:
        total = sum(shares.itervalues())
        dif = 100 - total
        if dif != 0:
            print "WARN: Incorrect shares. total={} dif={}".format(total, dif)
            max_key = max(shares.iteritems(), key=operator.itemgetter(1))[0]
            shares[max_key] += dif
        else:
            return shares


while True:
    try:
        kafka, redis = set_up_connections()
        break
    except Exception as e:
        print "ERROR: Waiting for Kafka and Redis...\n{}".format(e)
        time.sleep(5)

print "INFO: Kafka and Redis set up."
while True:
    try:
        print "INFO: Start reading kafka..."
        for message in kafka:
            report_values = ast.literal_eval(message.value)
            if len(report_values) == 0:
                print "WARN: Message is empty!"
                continue
            location = '0'
            kpi_score = report_values[0]['vidqoe']
            shares = ensure_shares(report_values[0]['shares'])
            print "INFO: New kafka message. {}".format(report_values)
            location = "0"
            result = redis.hgetall(location)
            print "INFO: Redis query for location \"{}\"={}".format(location, result)
            if len(result) == 0:
                # No session in database
                if kpi_score > config['kpi_critical_value']:
                    # Everyting allright
                    print "INFO: everthing OK"
                else:
                    # kpi < critical, POST
                    print "INFO: creating new session"
                    response, session_id = connect(method='POST', url=config['pgf_url'], shares=shares,
                                                   qoe_kpi=kpi_score, location=location)
                    redis.hmset(location, {'session_id': session_id})
                    print "INFO: session created. ID:{} Response:{}".format(session_id, response)
            else:
                # Session in database
                if kpi_score > config['kpi_critical_value']:
                    # Degradation aleviated, delete
                    print "INFO: deleting session..."
                    response, session_id = connect(method='DELETE', url=config['pgf_url'],
                                                   session_id=result['session_id'])
                    redis.delete(location)
                else:
                    # Degradation continued, PUT
                    print "INFO: updating session..."
                    response, session_id = connect(method='PUT', url=config['pgf_url'], shares=shares,
                                                   qoe_kpi=kpi_score, location=location,
                                                   session_id=result['session_id'])
                print "INFO: session updated. ID:{} Response:{}".format(session_id, response)

    except Exception as e:
        print "ERROR: Unkown error... {}".format(e)
