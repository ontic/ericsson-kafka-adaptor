from kafka import KafkaConsumer


class MyConsumer:
    def __init__(self, server='localhost:9092', topic="test"):
        self.consumer = KafkaConsumer(bootstrap_servers=server, auto_offset_reset='earliest')
        self.consumer.subscribe([topic])
