from pymongo import MongoClient
import time


def mongodbConnection(url="mongodb://localhost:27017/"):
    client = MongoClient(url)
    db = client.sessionsDB
    coll = db.sessionsColl
    return coll


# Checks if the sessionid received is valid.
def validation(location, sessionIdNumber, coll):
    cursor = coll.find({location + ".SessionID": sessionIdNumber})
    exists = False
    result = {}
    valid = False
    existing = False
    e = cursor.explain()
    if e["executionStats"]["nReturned"] > 0:
        for i in cursor:
            result = dict(i)
        id = result["_id"]
        print("The SessionID received in the given location was already in the database."
              " Checking Validity of the SessionID number")
        valid = checkValidityTime(id, location, coll)
        existing = True
    else:
        # If the location and index does not exist, we must insert a new record
        # we first have to check if the SessionID is existing for any other location
        print ("The SessionID received in the given location was not in the database."
               "Checking if the ID exists for any other location")
        # valid = idControl(location, sessionIdNumber, coll)
        valid = True
        existing = False
    return valid, existing


def deleteSession(id, coll):
    coll.delete_one({"_id": id})


def checkValidityTime(id, location, coll):
    cursor = coll.find({"_id": id})
    result = {}
    valid = False
    for i in cursor:
        result = dict(i)
    validityTime = result[location][1]["Validity"]
    currentTime = int(round(time.time() * 1000))
    if validityTime > currentTime:
        print("SessionID is Valid")
        valid = True
    else:
        print("SessionID is Not Valid")
        valid = False
        currentTime = int(round(time.time() * 1000))
        # drop invalid session
        coll.deleteMany({location + ".Validity": {"$gt": currentTime}})
        print("Deleting invalid SessionID")
    return valid


def insertSession(location, sessionID, coll):
    end = int(round(time.time() * 1000)) + 600 * 1000  # 600 seconds -> 10 minutes
    coll.insert_one({location:
        [
            {"SessionID": sessionID,
             "Validity": end}
        ]})


def updateSession(id, coll, location, sessionID):
    coll.update_many({"_id": id, location + ".SessionID": sessionID},
                     {"$set": {"Validity": int(round(time.time() * 1000))}})


def updateSession(coll, location, SessionID):
    doc = findLocation(coll, location)
    res = {}
    for i in doc:
        res = dict(i)
    id = res["_id"]
    print(res["_id"])
    coll.update({"_id": id}, {
        "_id": id,
        location: [
            {
                "SessionID": SessionID,
                "Validity": int(round(time.time() * 1000))
            }
        ]
    })


'''
def updateSession(id,coll,location,sessionID):
    coll.update_many({"_id":id, location+".SessionID":sessionID},{"$set": {"Validity" :int(round(time.time() * 1000))}})

 #Si uso esto:

    coll.update_many({"_id":id},{"$set": {location+".Validity" :int(round(time.time() * 1000))}})

    el error es el siguiente:

        mongo.updateSession(result["_id"],coll,location)#,result[location][0]["SessionID"])
  File "/Users/Guru/Ericsson/newforge/talentumQoE/talentumQoE/JSON_REST/mongodbHandlerv1.py", line 73, in updateSession
    coll.update_many({"_id":id},{"$set": {location + ".Validity" :int(round(time.time() * 1000))}})
  File "/Users/Guru/anaconda/lib/python2.7/site-packages/pymongo/collection.py", line 885, in update_many
    bypass_doc_val=bypass_document_validation)
  File "/Users/Guru/anaconda/lib/python2.7/site-packages/pymongo/collection.py", line 710, in _update
    _check_write_command_response([(0, result)])
  File "/Users/Guru/anaconda/lib/python2.7/site-packages/pymongo/helpers.py", line 301, in _check_write_command_response
    raise WriteError(error.get("errmsg"), error.get("code"), error)
pymongo.errors.WriteError: cannot use the part (0 of 0.Validity) to traverse the element ({0: [ { SessionID: "71ff0c87-4f89-4047-acff-ff04054d93f8", Validity: 1468249475896 } ]})

    y si uso

    coll.update_many({"_id":id},{"$set": {".Validity" :int(round(time.time() * 1000))}})

    mongo.updateSession(result["_id"],coll,location)#,result[location][0]["SessionID"])
  File "/Users/Guru/Ericsson/newforge/talentumQoE/talentumQoE/JSON_REST/mongodbHandlerv1.py", line 73, in updateSession
    coll.update_many({"_id":id},{"$set": {".Validity" :int(round(time.time() * 1000))}})
  File "/Users/Guru/anaconda/lib/python2.7/site-packages/pymongo/collection.py", line 885, in update_many
    bypass_doc_val=bypass_document_validation)
  File "/Users/Guru/anaconda/lib/python2.7/site-packages/pymongo/collection.py", line 710, in _update
    _check_write_command_response([(0, result)])
  File "/Users/Guru/anaconda/lib/python2.7/site-packages/pymongo/helpers.py", line 301, in _check_write_command_response
    raise WriteError(error.get("errmsg"), error.get("code"), error)
pymongo.errors.WriteError: The update path '.Validity' contains an empty field name, which is not allowed.



    con update solo tampoco funciona D:
    '''


def idControl(location, sessionIdNumber, coll):
    print("Accesing idcontrol")
    valid = False
    cursor = coll.find({})
    result = {}
    newID = 0
    for i in cursor:
        result = dict(i)

    for k, v in result.iteritems():
        try:
            if v[0]["SessionID"] == sessionIdNumber:
                print("SessionID found")
                print("Can't insert twice the same ID")
                valid = False

        except:
            pass
    return valid


def findLocation(coll, location):
    doc = coll.find({location: {"$exists": True}})
    return doc


'''
Database Name: sessionsDB
Collection Name: sessionsColl

'''
