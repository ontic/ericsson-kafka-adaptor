import httplib
import json
import time
import uuid

default_shares = {
    "gold": 20,
    "silver_corporate": 40,
    "silver_domestic": 5,
    "bronze_corporate": 5,
    "bronze_young": 30
}

default_resource = '/PGF_Server/rest/sessions'
default_method = 'POST'
default_url = 'localhost:8080'
default_qoe = 50
default_confidence = 1.0
default_location = '0'
header = {"Content-type": "application/json"}


def connect(session_id=None, method=default_method, url=default_url, shares=default_shares, resource=default_resource,
            qoe_kpi=default_qoe, confidence=default_confidence, location=default_location):
    timestamp = (time.time() * 1000)
    start_time = (time.time() + 1) * 1000
    end_time = (time.time() + 601) * 1000
    qoe_kpi = int(round(qoe_kpi))
    body = json.dumps({
        'reportID': str(uuid.uuid1()),
        'service': {
            'name': 'Video',
            'kpi': [
                {
                    'name': 'video_qoe',
                    'value': qoe_kpi
                }
            ]},
        'confidence': confidence,
        'timestamp': int(timestamp),
        'location': [location],
        'validity': {
            'start': int(start_time),
            'end': int(end_time)
        },
        'groups': [
            {"name": "gold", "share": int(shares["gold"])},
            {"name": "silver corporate", "share": int(shares["silver_corporate"])},
            {"name": "silver domestic", "share": int(shares["silver_domestic"])},
            {"name": "bronze corporate", "share": int(shares["bronze_corporate"])},
            {"name": "bronze young", "share": int(shares["bronze_young"])}
        ],
    })
    headers = {
        "Content-type": "application/json",
    }
    connection = httplib.HTTPConnection(url)
    if method == "POST":
        connection.request(method=method, url=resource, body=body, headers=headers)
    else:
        # PUT or DELETE
        connection.request(method=method, url=resource + str("/" + session_id), body=body, headers=headers)

    response = connection.getresponse()
    session_id = 0

    if response.reason == "Created":
        location = response.getheader('Location')
        pos = location.rfind("/")
        session_id = location[pos + 1:]

    connection.close()
    return response.reason, session_id
