
from kafkaProducer import MyProducer

cassandra_json = {
    "id": "1",
    "age": 12,
    "state": "miaumiaumiaumiaumiau"
}

items_list = []

if __name__ == "__main__":
    kafkaProducer = MyProducer(ip="159.107.25.109", topic="test")
    i = 1
    for i in range(10):
        global cassandra_json
        cassandra_json["id"] = str(i)
        global items_list
        items_list.append(cassandra_json)
    kafkaProducer.sendItems(items_list)