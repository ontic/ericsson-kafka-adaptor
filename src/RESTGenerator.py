import httplib
import json
import random
import string
import time
import uuid


def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def POST(url='ontic.extremeinnovationlab.net:8080', recurso='/PGF_Server/rest/sessions', vidqoe=0, confidence=0.5,
         location="0", goldShare=100, silvdomShare=0, brcorpShare=0, silvcorpShare=0, bryoungShare=0):
    t0 = (time.time() * 1000)
    t1 = (time.time() + 1) * 1000
    t2 = (time.time() + 601) * 1000
    vidqoe = int(round(vidqoe))
    params = json.dumps({
        'reportID': str(uuid.uuid1()),
        'service': {
            'name': 'Video',
            'kpi': [  ####PREGUNTAR!!!!
                {
                    'name': 'video_qoe',
                    'value': vidqoe
                }
            ]},
        'confidence': confidence,
        'timestamp': int(t0),
        'location': [location],
        'validity': {
            'start': int(t1),
            'end': int(t2)
        },
        'groups': [  # Verify with PGF DB
            {"name": "silver domestic", "share": int(silvdomShare)},
            {"name": "bronze corporate", "share": int(brcorpShare)},
            {"name": "silver corporate", "share": int(silvcorpShare)},
            {"name": "gold", "share": int(goldShare)},
            {"name": "bronze young", "share": int(bryoungShare)}
        ],
    }
    )
    print(params)
    headers = {
        "Content-type": "application/json",
    }
    conn = httplib.HTTPConnection(url)
    conn.request("POST", recurso, (params), headers)
    response = conn.getresponse()
    if (response.reason == "Created"):
        sessionID = response.getheader('Location')
        index = response.getheader('Location').find("content/")
        print(index)
        sessionReturn = sessionID[(len(url) + (index + 13)):len(sessionID)]
        print(index)
        sessionReturn = sessionID[(len(url) + (index + 13)):len(sessionID)]
        conn.close()
        return response.reason, sessionReturn
    else:
        conn.close()
        return response.reason, 0


def PUT(sessionID, url='ontic.extremeinnovationlab.net:8080', recurso='/PGF_Server/rest/sessions', vidqoe=0,
        confidence=0.5, location="0", goldShare=100, silvdomShare=0, brcorpShare=0, silvcorpShare=0, bryoungShare=0):
    t0 = (time.time() * 1000)
    t1 = (time.time() + 1) * 1000
    t2 = (time.time() + 601) * 1000
    vidqoe = int(round(vidqoe))
    params = json.dumps({
        'reportID': str(uuid.uuid1()),
        'service': {
            'name': 'Video',
            'kpi': [  ####PREGUNTAR!!!!
                {
                    'name': 'video_qoe',
                    'value': vidqoe
                }
            ]},
        'confidence': confidence,
        'timestamp': int(t0),
        'location': [location],
        'validity': {
            'start': int(t1),
            'end': int(t2)
        },
        'groups': [  # Verify with PGF DB
            {"name": "silver domestic", "share": int(silvdomShare)},
            {"name": "bronze corporate", "share": int(brcorpShare)},
            {"name": "silver corporate", "share": int(silvcorpShare)},
            {"name": "gold", "share": int(goldShare)},
            {"name": "bronze young", "share": int(bryoungShare)}
        ],
    }
    )
    print(params)
    headers = {
        "Content-type": "application/json",
    }
    conn = httplib.HTTPConnection(url)
    conn.request("PUT", recurso + str(sessionID), (params), headers)
    response = conn.getresponse()
    return response.status, response.reason


def DELETE(sessionID, url='ontic.extremeinnovationlab.net:8080', recurso='/PGF_Server/rest/sessions'):
    headers = {
        "Content-type": "application/json",
    }
    conn = httplib.HTTPConnection(url)
    conn.request(method="DELETE", url=recurso + str(sessionID), headers=headers)
    response = conn.getresponse()
    return response.status, response.reason
