#!/bin/bash

#################
#MongoDB linux 64
#################


sudo apt-get update

cd ~
echo '#################'
echo 'Installing pip'
echo '#################'
sudo apt-get install python-pip python-dev build-essential

echo '#################'
echo 'Downloading MongoDB'
echo '#################'
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.7.tgz
tar -zxvf mongodb-linux-x86_64-3.2.7.tgz
sudo mkdir -p ~/mongodb
sudo cp -R -n mongodb-linux-x86_64-3.2.7/ mongodb

echo 'export PATH=~/mongodb/bin:$PATH' >> ~/.bashrc 

sudo mkdir -p ~/mongodb/data/db



echo '#################'
echo 'Installing mongo library for python'
echo '#################'
sudo pip install pymongo

echo '#################'
echo 'Installing kafka library for python'
echo '#################'
sudo pip install kafka




