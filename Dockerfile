FROM ubuntu:14.04

RUN apt-get update && apt-get install -y \
    python-dev \
    python-pip \
    wget

RUN pip install \
    redis \
    kafka

RUN mkdir /pgf-wrapper
ADD src /pgf-wrapper

ADD entrypoint.sh /start-container
RUN chmod +x /start-container
ENTRYPOINT /start-container
