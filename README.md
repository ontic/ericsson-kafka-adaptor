Adaptor between a Kafka Message Broker and and the PGF REST interface (IF3-2). 
It encapsulates the QoE insights sent from the [Analytics Function](https://gitlab.com/ontic/ericsson-af) 
through the Kafka Message Broker and implements the RESTful client able to 
connect to the [PGF](https://gitlab.com/ontic/ericsson-pgf-server). 
It also implements a permanent storage on a NoSQL database, Mongodb, to keep 
track of ongoing QoE degradation sessions.


The image is available in [Docker Hub](https://hub.docker.com/r/onticericssonspain/kafka-adaptor/) 
and can to be executed in a [Docker Compose environment](https://gitlab.com/ontic/ericsson-uc3-demo2)
